//Money earned after wage
const addPay = 1000; //Task said 100 but 1000 is easier for testing. Change if you want :)
//Dom Buttons
const elLoanBtn = document.getElementById('btn-loan');
const elBankBtn = document.getElementById('btn-bank');
const elWorkBtn = document.getElementById('btn-work');
const elBuyBtn = document.getElementById('btn-buy');
//Output money stuff
const elBalance = document.getElementById('balance');
const elPay = document.getElementById('pay');
//Output Laptop info
const elLaptops = document.getElementById('laptops');
const elPic = document.getElementById('pic'); 
const elName = document.getElementById('name');
const elDesc = document.getElementById('desc');
const elPrice = document.getElementById('price');
const elFeatures = document.getElementById('features');

//Files for images
fileMac =     "./images/Macbook_Pro.jpg";
fileXPS =     "./images/Dell_XPS.jpg";
fileSpectre = "./images/HP_Spectre.jpg";
fileMate =    "./images/Matebook_Pro.jpg";

//Arrays with features for each PC. 
const fMac = ['Apple', 'iOS', 'Brand'];
const fXPS = ['Windows', 'All Arounder'];
const fSpectre = ['Windows', 'Slim with normal processor', 'Beautiful'];
const fMate = ['Windows?', 'Almost borderless screen'];
const laptopF = [fMac, fXPS, fSpectre, fMate];

//Global variables
let balance = 0;
let pay = 0;
let hasLoan = false;

function change(selectObject){
    let id;
    elPic.innerHTML = '';
    elFeatures.innerHTML = '';
    const v = selectObject.value;
    let [ name,price ] = v.split(", ");
    const img = document.createElement("img");
    elName.innerHTML = name;
    elPrice.innerHTML = price + ' NOK';

    //This couldve been a lot more efficient, but since we only have 4 pcs i went with this.
    if(name == 'Macbook Pro'){
        id = 0;
        img.src = fileMac;
        elDesc.innerHTML = "Buy this because its Apple";
    }
    else if(name == 'Dell XPS'){
        id = 1;
        img.src = fileXPS;
        elDesc.innerHTML = "Considered the best PC by many";
    }
    else if(name == 'HP Spectre'){
        id = 2;
        img.src = fileSpectre;
        elDesc.innerHTML = "Probably the best looking computer";
    }
    else if(name == 'Huawei Matebook'){
        id = 3;
        img.src = fileMate;
        elDesc.innerHTML = "The Apple of Windows";
    }
    addFeatures(id);
    elPic.append(img);
}

elLoanBtn.addEventListener('click', function(event){
    let loan = window.prompt("How much loan do you want: ");
    console.log(loan)
    if(loan > balance + balance){
        alert("You cannot loan more than 2x balance");
    }

    else{
        if(hasLoan === false){
            balance += parseInt(loan);
            hasLoan = true;
            render();
        }
        else{
            alert("You can only take loan once for a Laptop. Buy one first");
        }
    }
});

elBankBtn.addEventListener('click', function(event){
    balance += pay;
    pay = 0;
    render();
});

elWorkBtn.addEventListener('click', function(event){
    pay += addPay;
    render();
});

elBuyBtn.addEventListener('click', function(event){
    const currentLaptop = elLaptops.value;
    let [ name,price ] = currentLaptop.split(", ");
    price = parseInt(price);

    if(balance >= price){
        balance -= price;
        hasLoan = false;
        alert(name + ' has been purchased for ' + price + ' NOK' );
    }

    else{
        alert("Insufficent funds");
    }
    render();

});

function render(){
    elBalance.innerHTML = balance;
    elPay.innerHTML = pay;
}

function addFeatures(id){
    let list = document.createElement('p');
    for (const feature of laptopF[id]){
        let item = document.createElement('li');
        item.appendChild(document.createTextNode(feature));
        list.appendChild(item);
    }
    elFeatures.appendChild(list);
}